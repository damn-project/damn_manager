DAMN_SERVER = "https://server.damn-project.org"

/*! Return damn server endpoint
*/
function ep(e)
{
    return DAMN_SERVER+e;
}

/*! Update div's innerHTML
*/
function up(i, w)
{
    document.getElementById(i).innerHTML = w;
}

/*! Send AJAJ request

\see https://en.wikipedia.org/wiki/JSON#AJAJ

\param cb Callback function with obj parameter.
\param url Request URL.
*/
function get_ajaj(cb, url)
{
    var http_request = new XMLHttpRequest();
    http_request.open("GET", url, true);
    http_request.responseType = "json";
    http_request.onreadystatechange = function () {
        var done = 4, ok = 200;
        if (http_request.readyState === done && http_request.status === ok) {
            cb(http_request.response);
        }
    };
    http_request.send(null);
}
function post_ajaj(cb, obj, url)
{
    var http_request = new XMLHttpRequest();
    http_request.open("POST", url, true);
    http_request.setRequestHeader("Content-Type", "application/json");
    http_request.setRequestHeader("Authorization", "Bearer " + get_token());
    http_request.responseType = "json";
    http_request.onreadystatechange = function() {
        var done = 4, ok = 201;
        if (http_request.readyState === done && http_request.status === ok) {
            cb(http_request.response);
        }
    };
    http_request.send(JSON.stringify(obj));
}
function put_ajaj(cb, obj, url)
{
    var http_request = new XMLHttpRequest();
    http_request.open("PUT", url, true);
    http_request.setRequestHeader("Content-Type", "application/json");
    http_request.setRequestHeader("Authorization", "Bearer " + get_token());
    http_request.responseType = "json";
    http_request.onreadystatechange = function() {
        var done = 4, ok = 200;
        if (http_request.readyState === done && http_request.status === ok) {
            cb(http_request.response);
        }
    };
    http_request.send(JSON.stringify(obj));
}

/*! Get token from cookie.
*/
function get_token()
{
    var token;
    try {
        var t = document.cookie.match(/token=(.*)/);
        token = RegExp.$1;
    } catch (e) {
        token = false;
    }
    return token;
}

/*! Decode JWT

\see https://stackoverflow.com/questions/38552003/how-to-decode-jwt-token-in-javascript-without-using-a-library#38552302

\param token JSON Web Token.
*/
function get_jwt(token)
{
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var payload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' +
        c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(payload);
}

/*! Decode user from JWT stored in cookie.
*/
function get_user()
{
    var u;
    try {
        var t = get_token();
        u = get_jwt(t);
    } catch (e) {
        u = false;
    }
    return u;
}

/*! Show menu
*/
function show_menu()
{
    var ih = "";
    ih += "<div class='button' onclick='get_areas()'>";
    ih += "Get list of areas";
    ih += "</div>";
    ih += "<div class='button' onclick='create_area()'>";
    ih += "Add area";
    ih += "</div>";
    var u = get_user();
    if (u) {
        ih += "<div class='button' onclick=''>"; // TODO change tags form
        ih += "I am " + u["display_name"] + ", my tags: " + u["tags"];
        ih += "</div>";
    } else {
        ih += "<div class='button' onclick='get_auth()'>";
        ih += "Authenticate to OpenStreetMap";
        ih += "</div>";
    }
    up("main", ih);
}

// API
/*! Get authorization token
*/
function get_auth()
{
    var chs = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var tid = "";
    for (var i = 0; i < 64; i++)
        tid += chs.charAt(Math.floor(Math.random() * chs.length));
    var child = window.open(DAMN_SERVER+"/auth/"+tid);
    var check = setInterval(function() {
        get_ajaj(
            function(obj) {
                if (!("token" in obj))
                    return;
                clearTimeout(check);
                c = "token=" + obj["token"];
                var d = new Date();
                d.setFullYear(d.getFullYear() + 1);
                c += "; expires=" + d.toUTCString();
                document.cookie = c;
                location.reload();
            },
            ep("/token/"+tid),
        );
    }, 100);
}

// -- areas --
/*! Get areas
*/
function get_areas()
{
    get_ajaj(
        function(obj) {
            var ih = "";
            for (a in obj) {
                ih += area_row(obj[a]);
            }
            up("main", ih);
        },
        ep("/areas"),
    );
}

/*! Convert JSON area to HTML card
*/
function area_row(area)
{
    var nof_squares = area["squares_to_map"];
    nof_squares += area["squares_to_review"];
    nof_squares += area["squares_done"];
    var to_map = 100 * area["squares_to_map"] / nof_squares;
    var to_rev = 100 * area["squares_to_review"] / nof_squares;
    var done = 100 * area["squares_done"] / nof_squares;
    var row = "<div";
            row += " class='row text'";
            row += " onclick='get_area(" + area["aid"] + ")'";
            row += " title='AID (Priority) (To map/To review/Done): Tags'"
        row += ">";
        row += area["aid"];
        row += " (" + area["priority"] + ")";
        row += " (" + to_map;
        row += "/" + to_rev;
        row += "/" + done;
        row += ")";
        row += ": " + area["tags"];
    row += "</div>";
    return row;
}

// -- area --
/*! Get area details, stats, and commits
*/
function get_area(aid)
{
    var ih = "<div id='info'>";
        ih += "<div id='details'></div>";
    ih += "</div>";
    ih += "<div id='commits'></div>";
    up("main", ih);
    get_ajaj(
        function(obj) {
            up("details", area_details(obj));
        },
        ep("/area/"+aid),
    );
    get_ajaj(
        function(obj) {
            var ih = "";
            ih += "<h2>Commits</h2>";
            for (c in obj)
                ih += commit_commit(obj[c]);
            up("commits", ih);
        },
        ep("/area/"+aid+"/commits"),
    );
}

/*! Convert JSON area to HTML details
*/
function area_details(area)
{
    var ids = [];
    var d = "";
    d += "<h2>AID</h2>";
    d += area["aid"];
    d += "<input id='aid' type='hidden' value='" + area["aid"] + "' />";
    d += "<h2>Priority</h2>"
    d += "<input";
        d += " id='priority'";
        d += " type='text'";
        d += " value='" + area["priority"] + "'";
        d += " title='Priority'";
    d += " />";
    ids.push('priority');
    d += "<h2>Tags</h2>";
    d += "<textarea id='tags' title='Tags'>" + area["tags"] + "</textarea>";
    ids.push('tags');
    d += "<h2>Description</h2>";
    for (var l in area["description"]) {
        d += "<input";
            d += " id='lc " + l + "'";
            d += " type='text'";
            d += " value='" + l + "'";
            d += " title='Language code'";
        d += " />";
        ids.push('lc ' + l);
        d += "<textarea id='desc " + l + "' title='Description'>";
            d += area["description"][l];
        d += "</textarea>";
        ids.push('desc ' + l);
    }
    d += "<h2>Instructions</h2>";
    var instructions = [];
    for (var i in area["instructions"]) {
        instructions.push(i);
        d += "<input";
            d += " id='instr1 " + i + "'";
            d += " type='text'";
            d += " value='" + i + "'";
            d += " title='Instruction'";
        d += " />";
        ids.push('instr1 ' + i);
        d += "<textarea id='instr2 " + i + "' title='Instruction description'>";
            d += area["instructions"][i];
        d += "</textarea>";
        ids.push('instr2 ' + i);
    }
    d += "<h2>Update</h2>";
    d += "<div";
        d += " class='button'";
        d += " onclick='update_area(" + JSON.stringify(ids) + ")'";
    d += ">";
        d += "Update area";
    d += "</div>";
    return d;
}

function update_area(ids)
{
    var area = {};
    area["aid"] = document.getElementById("aid").value;
    area["tags"] = document.getElementById("tags").value;
    area["priority"] = document.getElementById("priority").value;
    area["description"] = {};
    for (var i in ids) {
        if (ids[i].indexOf("desc ") == 0) {
            var lang = ids[i].slice(5);
            var lc = document.getElementById("lc " + lang).value;
            var desc = document.getElementById("desc " + lang).value;
            area["description"][lc] = desc;
        }
    }
    area["instructions"] = {};
    for (var i in ids) {
        if (ids[i].indexOf("instr") == 0) {
            var what = ids[i].slice(7);
            var i1 = document.getElementById("instr1 " + what).value;
            var i2 = document.getElementById("instr2 " + what).value;
            area["instructions"][i1] = i2;
        }
    }
    put_ajaj(
        function(obj) { get_area(obj["aid"]); },
        area,
        ep("/area/"+area["aid"]),
    );
}

function create_area()
{
        var ih = "";
        ih += "<h2>AID</h2>";
        ih += "Will be assigned automatically.";
        ih += "<h2>Priority</h2>";
        ih += "<input";
            ih += " id='priority'";
            ih += " type='text'";
            ih += " value='1'";
            ih += " title='Priority'";
        ih += " />";
        ih += "<h2>Tags</h2>";
        ih += "<textarea id='tags' title='Tags'></textarea>";
        ih += "<h2>Description</h2>";
        ih += "<input";
            ih += " id='lang'";
            ih += " type='text'";
            ih += " value='en'";
            ih += " title='Language code'";
        ih += " />";
        ih += "<textarea id='desc' title='Description'></textarea>";
        ih += "<h2>Instructions</h2>";
        ih += "<div><ul id='instructions'></ul></div>";
        ih += "<a href='javascript:add_instruction()'>Add instruction</a>";
        ih += "<h2>Area to map</h2>";
        ih += "<p>";
        ih += "This has to be geojson feature collection with polygons.";
        ih += "Create one at <a href='http://geojson.io/' target='_blank'>";
            ih += "http://geojson.io/";
        ih += "</a>.";
        ih += "</p>";
        ih += "<input id='featurecollection' type='file' />";
        ih += "<h2>Create</h2>";
        ih += "<div";
            ih += " class='button'";
            ih += " onclick='post_area()'";
        ih += ">";
            ih += "Create area";
        ih += "</div>";
        up("main", ih);
}
function add_instruction()
{
    var ul = document.getElementById("instructions");
    var li = document.createElement("li");
    var t1 = document.createElement("input");
    t1.type = "text";
    t1.title = "Instruction";
    t1.id = "i1 " + ul.childElementCount;
    var t2 = document.createElement("textarea");
    t2.type = "text";
    t2.title = "Instruction description";
    t2.id = "i2 " + ul.childElementCount;
    li.appendChild(t1);
    li.appendChild(t2);
    ul.appendChild(li);
}
function post_area()
{
    var area = {};
    area["tags"] = document.getElementById("tags").value;
    area["priority"] = Math.floor(document.getElementById("priority").value);
    var desc_lang = document.getElementById("lang").value;
    var desc = document.getElementById("desc").value;
    area["description"] = {};
    area["description"][desc_lang] = desc;
    var fc_source = document.getElementById("featurecollection").files[0];
    area["instructions"] = {};
    var icnt = 0;
    while (1) {
        try {
            i1 = document.getElementById("i1 " + icnt).value;
            i2 = document.getElementById("i2 " + icnt).value;
            area["instructions"][i1] = i2;
            icnt += 1;
        } catch(e) {
            break;
        }
    }
    var fr = new FileReader();
    fr.onload = function(e) {
        area["featurecollection"] = JSON.parse(e.target.result);
        post_ajaj(
            function(obj) { get_area(obj["aid"]); },
            area,
            ep("/areas"),
        );
    };
    fr.readAsText(fc_source);
}

/*! Convert JSON commit to HTML commit
*/
function commit_commit(commit)
{
    var html = "";
        html += "<div class='commit'";
        if (commit["sid"]) {
            html += " onclick='get_square(";
            html += commit["aid"];
            html += ", ";
            html += commit["sid"];
            html += ")";
        }
        html += "'>";
        html += "<div class='commit_message text'>";
        html += commit["message"];
        html += "</div>";
        html += "<div class='commit_sign text'>";
        var d = new  Date(commit["date"]);
        html += commit["author"];
        html += ", " + d.toLocaleString();
        html += ", " + commit["aid"] + "/" + commit["sid"];
        html += "</div>";
    html += "</div>";
    return html;
}
