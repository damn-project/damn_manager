# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased][]

## [0.1.1][] - 2020-10-01
### Fixed
- Update to new server (v0.6.0) API.

## 0.1.0 - 2020-01-01
### Added
- Changelog, license, readme.
- Add basic functions from [damn_client][].
- Create area function.

[damn_client]: https://gitlab.com/damn-project/damn_client

[Unreleased]: https://gitlab.com/damn-project/damn_manager/compare/v0.1.1...master
[0.1.1]: https://gitlab.com/damn-project/damn_manager/compare/v0.1.0...v0.1.1
